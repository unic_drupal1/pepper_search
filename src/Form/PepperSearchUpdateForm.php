<?php

namespace Drupal\pepper_search\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Pepper_search Update form.
 */
class PepperSearchUpdateForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['title'] = [
      '#type' => 'item',
      '#title' => 'Update Search Index',
      '#value' => $this->t('Update Search Index'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => t('Update'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $rebuildTrigger = \Drupal::service('pepper_search.search_update_trigger');
    $rebuildTrigger->triggerRebuild();
    $this->messenger()->addStatus($this->t('Index has been updated'));
  }

  /**
   * Get editable config names.
   */
  protected function getEditableConfigNames() {
    // @todo Implement getEditableConfigNames() method.
  }

  /**
   * Get FormId.
   */
  public function getFormId() {
    return 'pepper_search_update_form';
  }

}
