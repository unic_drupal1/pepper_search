<?php

namespace Drupal\pepper_search\Service;

use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Site\Settings;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LogLevel;

/**
 * Class to trigger search indexing.
 *
 * @var $client
 * @var $logger
 */
class SerachIndexUpdateTrigger {

  protected $client;

  protected $logger;

  /**
   * Constructor.
   */
  public function __construct(ClientFactory $client_factory, LoggerChannelFactoryInterface $loggerChannelFactory) {
    $this->client = $client_factory->fromOptions([
      'verify' => FALSE,
    ]);
    $this->logger = $loggerChannelFactory->get('pepper_search');
  }

  /**
   * Trigger the content Indexing.
   */
  public function triggerRebuild() {
    // Check if the service url is configured.
    $serviceUrl = Settings::get('content_rebuild_trigger.service_url');
    $this->logger->log(LogLevel::INFO, 'Trigger content rebuild service at: ' . $serviceUrl);
    if ($serviceUrl) {
      try {
        $this->logger->log(LogLevel::INFO, 'Trigger content rebuild now (' . $serviceUrl . ')');
        $this->client->post($serviceUrl, []);
      }
      catch (GuzzleException $e) {

      }
    }
  }

}
